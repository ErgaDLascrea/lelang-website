<?php  ?>
<div id="top">
    <nav class="navbar navbar-inverse navbar-fixed-top "
        style="padding-top: 10px;padding-bottom: 10px; background-color: lightblue;">
        <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip"
            class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu"
            id="menu-toggle">
            <i class="icon-align-justify"></i>
        </a>
        <header class="navbar-header">
            <a href="<?php echo base_url(); ?>" class="navbar-brand">
                <img src="<?php echo base_url('foto/logo1.png'); ?>" alt="" width="50px" />
            </a>
        </header>
        <h2 style="text-align: center;">DATA PENDUDUK </h2>
    </nav>
</div>
<br><br>
<br><br>
<div class="container">
    <div id="left" style="position: fixed;">
        <div class="media user-media well-small">
            <a class="user-link" href="<?php echo site_url('c_admin'); ?>">
                <img class="media-object img-thumbnail user-img" alt="User Picture"
                    src="<?php echo base_url('foto_user_admin/' . $dataAdmin->foto_profil); ?>" width="50px" />
            </a>
            <br />
            <div class="media-body">
                <h5 class="media-heading"> <?php echo $dataAdmin->Nama; ?></h5>
                <li><a href="<?php echo site_url('c_admin/showEdit'); ?>"><i class="icon-user"></i> Edit admin
                        Profile </a>
                </li>
            </div>
            <br />
        </div>

        <ul id="menu" class="collapse">
            <li class="panel <?php if ($jenisKlik == 'profil') {
                                    echo "active";
                                } ?>">
                <a href="<?php echo site_url('c_admin'); ?>">
                    <i class="glyphicon glyphicon-user"></i> Profil
                </a>
            </li>
            <li class="panel <?php if ($jenisKlik == 'penduduk') {
                                    echo "active";
                                } ?>">
                <a href="<?php echo site_url('c_admin/showPenduduk'); ?>">
                    <i class="glyphicon glyphicon-th-list"> </i>Manage Penduduk
                </a>
            </li>
            <li class="panel <?php if ($jenisKlik == 'pegawai') {
                                    echo "active";
                                } ?>">
                <a href="<?php echo site_url('c_admin/showPegawai'); ?>">
                    <i class="icon-pencil"></i> Manage Pengurus
                </a>
            </li>
            <li class="panel <?php if ($jenisKlik == 'pengumuman') {
                                    echo "active";
                                } ?>">
                <a href="<?php echo site_url('c_admin/showPengumuman'); ?>">
                    <i class="icon-table"></i> Manage Pengumuman
                </a>
            </li>
            <li class="panel <?php if ($jenisKlik == 'draft') {
                                    echo "active";
                                } ?>">
                <a href="<?php echo site_url('c_admin/showDraft'); ?>">
                    <i class="glyphicon glyphicon-envelope"></i> Draft Pengajuan
                </a>
            </li>

            <li class="panel <?php if ($jenisKlik == 'history') {
                                    echo "active";
                                } ?>">
                <a href="<?php echo site_url('c_admin/showHistory'); ?>">
                    <i class="glyphicon glyphicon-envelope"></i> History Pengajuan
                </a>
            </li>
            <li class="panel <?php if ($jenisKlik == 'buat') {
                                    echo "active";
                                } ?>">
                <a href="<?php echo site_url('c_admin/showAddFixSurat'); ?>">
                    <i class="glyphicon glyphicon-pencil"></i> Buat Surat
                </a>
            </li>
            <li class="panel"></li>
            <li><a href="<?php echo site_url('c_web/logout'); ?>"><i class="icon-signout"></i> Logout </a>
            </li>
        </ul>
    </div>