<?php  ?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url('asset/bootstrap/css/bootstrap.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/bootstrap/css/bootstrap.min.css'); ?>">

    <script type="text/javascript" src="<?php echo base_url('asset/bootstrap/js/bootstrap.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/bootstrap/js/jquery.min.js'); ?>"></script>
    <title>Daduk</title>
    <link rel="shorcut icon" href="<?php echo base_url() . 'foto/icon.png' ?>">
    <style>
    <?php include "style2.css";
    ?>
    </style>

</head>

<body>
    <div class="container-fluid header">
        <div class="row">

            <div class="col-md-2">
                <a href="<?php echo base_url(); ?>"><img class="logo" src="<?php echo base_url('foto/logo1.png'); ?>"
                        alt="logo"></a>
            </div>

            <div class="col-md-10 text-right">
                <?php if ($this->session->userdata('status') == "loginUser" || $this->session->userdata('status') == "loginAdmin") { ?>
                <a href="<?php if ($this->session->userdata('status') == 'loginUser') {
                                    echo site_url('c_user');
                                } else {
                                    echo site_url('c_admin');
                                } ?>" style=" text-decoration: none; ">
                    <button type="button" class="btn btn-light" style="background-color:  #007bff;">Profil</button>
                </a>
                <a href="<?php echo site_url('c_web/logout'); ?>" style=" text-decoration: none; ">
                    <button type="button" class="btn btn-light" style="background-color:  #007bff;">Logout</button>
                </a>
                <?php } else { ?>

                <a href="<?php echo site_url(''); ?>" style=" text-decoration: none; ">
                    <button type="button" class="btn btn-light" style="background-color:  #007bff;">Home</button>
                </a>
                <a href="<?php echo site_url('c_web/showLogin/' . 'Login User'); ?>" style=" text-decoration: none; ">
                    <button type="button" class="btn btn-light" style="background-color:  #007bff;">Login User</button>
                </a>
                <a href="<?php echo site_url('c_web/showLogin/' . 'Login Admin'); ?>" style=" text-decoration: none; ">
                    <button type="button" class="btn btn-light" style="background-color: #007bff;">Login Admin</button>
                </a>

                <?php } ?>

            </div>

        </div>
        <p style="font-family: center; font-size:24px; color:#000000; text-align: center; padding-top : 40px"> SELAMAT
            DATANG DI
            WEBSITE RT 009 RW 014 KELURAHAN JATIKRAMAT KECAMATAN JATI ASIH KOTA BEKASI</p>
    </div>

</body>

</html>