      <div id="heading-breadcrumbs">
        <div class="container">
          <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
              <h1 class="h2">About Us</h1>
            </div>
            <div class="col-md-5">
              <ul class="breadcrumb d-flex justify-content-end">
                <li class="breadcrumb-item"><a href="/lelang">Home</a></li>
          
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div id="content">
        <div class="container">
          <section class="bar">
            <div class="row">
              <div class="col-lg-12">
                <div class="heading">
                  <h2>Prosesi Lelang</h2>
                </div>
      
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8">
                <div id="accordion" role="tablist">
                  <div class="card">
                    <div id="headingOne" role="tab" class="card-header">
                      <h4 class="mb-0 mt-0"><a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Langkah Pertama</a></h4>
                    </div>
                    <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" class="collapse show">
                      <div class="card-body">
                        <p>Log-in dahulu, jika belum memiliki akun agar segera mendaftarkan akunnya</p>
                        <p></p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div id="headingTwo" role="tab" class="card-header">
                      <h4 class="mb-0 mt-0"><a data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed">Langkah Kedua</a></h4>
                    </div>
                    <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" class="collapse">
                      <div class="card-body">
                        <p>Menetapkan harga barang untuk harga awal dan margin harga setiap bid/tawaran</p>
                        <p></p>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div id="headingThree" role="tab" class="card-header">
                      <h4 class="mb-0 mt-0"><a data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed">Langkah Ketiga</a></h4>
                    </div>
                    <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion" class="collapse">
                      <div class="card-body">
                        <p>Jika harga sudah sesuai, konfirmasi harga dengan menggunakan fitur " DEAL "</p>
                        <p></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
 
             
        </section>
        <section class="bar background-pentagon mb-0">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                
              
                        
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
             
              </div>
            </div>
          </div>
        </section>
      </div>